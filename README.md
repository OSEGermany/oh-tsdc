<!--
SPDX-FileCopyrightText: 2021 Martin Häuer <martin.haeuer@ose-germany.de>
SPDX-FileCopyrightText: 2021 - 2024 Robin Vobruba <hoijui.quaero@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# Technology-specific Documentation Criteria for Open Source Hardware

[![License: GPL-3.0-or-later](
  https://img.shields.io/badge/License-GPL--3.0--or--later-blue.svg)](
  https://www.gnu.org/licenses/gpl-3.0)
[![REUSE status](
  https://api.reuse.software/badge/gitlab.com/OSEGermany/oh-tsdc)](
  https://api.reuse.software/info/gitlab.com/OSEGermany/oh-tsdc)

This repository contains Technology-specific Documentation Criteria (TsDC)
according to [DIN SPEC 3105-1].

You will find here:

- a document stating the requirements (and linking well-done examples):
  [OH-TsDC.md](OH-TsDC.md) \
  ↑ that's what you're looking for in most cases
- machine-readable Ontology (Linked Open Data, RDF/Turtle format)
  to reference these requirements:
  [oh-tsdc.ttl](oh-tsdc.ttl) & [tsdc-req.ttl](tsdc-req.ttl) \
  ↑ used e.g. for standardised metadata for OSH
  (see the [OKH] repo)
- Human-readable representations of the before-mentioned Ontology (generated): \
  <https://osegermany.gitlab.io/oh-tsdc/>

## Brief Description

- Technology-specific Documentation Criteria (TsDC)
  specify the requirements for the technical documentation
  of Open Source Hardware (OSH).
- A TsDC is created (so far manually) by OSH projects/developers.
  - Current community-based assessment program according to DIN SPEC 3105-2:
    [Conformity Assessment Body by OSE Germany e.V.][CAB]
- Requirements are organised in modules.
  All modules are listed in the TsDC database ([TsDC-DB].
  - A TsDC is thus **a subset** of [TsDC-DB]
- The concept of TsDC was initially mentioned in DIN SPEC 3105-1 (since v0.3)
  and probably will be mainly used in this context.
- Target group of requirements: Specialists
  - **In one word: Build and operate at your own risk.**
  - Certified technical documentation (accourding to DIN SPEC 3105-2)
    shall deliver sufficient information to enable (at least) professionals
    to reproduce, operate, maintain and dispose the documented hardware.
    Thus this hardware:
    - is not meant to be (commercially) distributed
      _without_ previous professional inspection;
    - is meant to run in uncritical environments regarding safety.

## Related Standards

- [DIN SPEC 3105-1]
- [DIN SPEC 3105-2]
- [Open Know-How][OKH]

# Fine, how can I use it?

1. Get a (digital or printed) copy of
    1. [TsDC-print](TsDC-print.md)
    2. [TsDC-Questionnaire-print](TsDC-Questionnaire-print.md)
2. In `TsDC-Questionnaire-print`, tick the boxes that match your case.
    1. Note down the corresponding ID when ticking a box.\
        _e.g._ `ASM-PCB` _or_ `COM-MAN`
    2. When ticking boxes in subordinated questions,
        also tick box of the corresponding superordinate question.\
        _e.g. tick "any other assembly" when ticking "…including welded components"
3. In `TsDC-print`, copy the rows under each ID you noted down in step 2.
    The result is your specific TsDC
    1. Each row states a requirement.
    2. Requirements marked with `M` are mandatory,
      `T` mandatory if necessary for the technical design,
      `B` are (optional) best practices
4. Just to make this clear (again),
    to quote from [DIN SPEC 3105-1](
    https://gitlab.com/OSEGermany/OHS-3105/-/blob/ohs/DIN_SPEC_3105-1.md#53-access):
    __all__ information required both in the standard and the TsDC is to be delivered:
    1. in its original editable file format and
    2. in an export file format that:
        1. is of well-established use in the corresponding field of technology,
        2. can be processed by software that is generally accessible to the recipients and
        3. contains no less information than the original editable file format.

The columns of `TsDC-print` explained:

Column|Explanation
---|---
ID|ID found in `TsDC-Questionnaire-print`
MODULE|name of the module of requirements; human-readable ID
INFORMATION|information to be delivered by the documentation
COMMON SOURCE FILE|**suggestion** _how_ this information is usually delivered
M|mandatory
T|mandatory if necessary for the technical design
B|best practice

[TsDC-DB]: TsDC-print.md
[CAB]: https://gitlab.opensourceecology.de/verein/projekte/cab/CAB#oseg-conformity-assessment-body
[DIN SPEC 3105-1]: https://gitlab.com/OSEGermany/OHS-3105/-/blob/ohs/DIN_SPEC_3105-1.md
[DIN SPEC 3105-2]: https://gitlab.com/OSEGermany/OHS-3105/-/blob/ohs/DIN_SPEC_3105-2.md
[OKH]: https://github.com/iop-alliance/OpenKnowHow/
